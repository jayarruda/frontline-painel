#!/bin/bash

#####   NOME:               install.sh
#####   VERSÃO:             1.0.4
#####   DESCRIÇÃO:          Instala os serviços necessários à criação de painel de senha Frontline.
#####   DATA DA CRIAÇÃO:    19/07/2022
#####   ÚLTIMA ATUALIZAÇÃO: 19/07/2022
#####   ESCRITO POR:        Junior A. S. Arruda
#####   E-MAIL:             junior@datapro.net.br
#####   DISTRO:             Ubuntu Server 22.04 (LTS)
#####   LICENÇA:            GPLv3
#####   PROJETO:            https://gitlab.com/jayarruda/frontline-painel.git

# Define colors
red='\e[0;31m'
green='\e[1;32m'
blue='\e[1;36m'
NC='\e[0m' # No color

clear


# Determine Ubuntu Version Codename
VERSION=$(lsb_release -cs)

# Check if stages.cfg exists. If not, created it. 
if [ ! -f stages.cfg ]
then
echo 'updates_installed=0
timezone_installed=0
grub_recovery_disable=0
wireless_enabled=0
essencial_installed=0
xorg_installed=0
admin_created=0
kiosk_created=0
kiosk_autologin=0
screensaver_installed=0
firefox_installed=0
kiosk_scripts=0
mplayer_installed=0
touchscreen_installed=0
audio_installed=0
additional_software_installed=0
crontab_installed=0
prevent_sleeping=0
ossh_installed=0
kiosk_permissions=0
immagemagick_installed=0
openvpn_installed=0' > stages.cfg
fi

# Import stages config
. stages.cfg




echo -e "${red}Installing operating system updates ${blue}(this may take a while)${red}...${NC}"
if [ "$updates_installed" == 0 ]
then
# Refresh
sudo apt-get -y update        # Fetches the list of available updates
sudo apt-get -y dist-upgrade  # Installs updates (new ones)
sudo apt-get -y upgrade       # Strictly upgrades the current packages

# Clean
apt-get -q=2 autoremove
apt-get -q=2 clean
sed -i -e 's/updates_installed=0/updates_installed=1/g' stages.cfg
clear
echo -e "${red}Installing operating system updates... ${NC}"
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Updates already installed. Skipping...${NC}"
fi

read -p "Press any key to continue... ${NC}" -n1 -s

echo -e "${red}Configuring timezone ${blue}(this may take a while)${red}...${NC}"
if [ "$timezone_installed" == 0 ]
then
# Timezone
hwclock --hctosys
dpkg-reconfigure tzdata
service ntp restart
apt-get -o Acquire::Check-Valid-Until=false -o Acquire::Check-Date=false update
sed -i -e 's/timezone_installed=0/timezone_installed=1/g' stages.cfg
clear
echo -e "${red}Configuring timezone... ${NC}"
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Timezone already configured. Skipping...${NC}"
fi

read -p "Press any key to continue... ${NC}" -n1 -s

echo -e "${red}Installing essencial packages...${NC}"
if [ "$essencial_installed" == 0 ]
then
apt-get -q=2 install apt-utils net-tools dialog git dnsutils iputils-ping ntp dialog make apt-utils vim plymouth-themes pulsemixer cron > /dev/null
systemctl disable snapd.service
systemctl disable snapd.socket
systemctl disable snapd.seeded.service
snap remove --purge firefox
snap remove --purge snap-store
snap remove --purge gnome-3-38-2004
snap remove --purge gtk-common-themes
snap remove --purge snapd-desktop-integration
snap remove --purge bare
snap remove --purge core20

rm -rf /var/cache/snapd/
apt remove --autoremove snapd
rm -rf ~/snap
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/config/nosnap.pref -O /etc/apt/nosnap.pref
apt update
read -p "Press any key to continue... " -n1 -s
else
	echo -e "${blue}Essencial already installed. Skipping...${NC}"
fi

echo -e "${red}Installing a graphical user interface...${NC}"
if [ "$xorg_installed" == 0 ]
then
apt-get -q=2 install --no-install-recommends xorg matchbox-window-manager > /dev/null
apt-get -q=2 install --no-install-recommends nodm
read -p "Press any key to continue... " -n1 -s

echo -e "${red}Installing openssh...${NC}"
if [ "$ossh_installed" == 0 ]
then
apt-get -q=2 install --no-install-recommends openssh-server > /dev/null
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/sshd_config -O /etc/ssh/sshd_config
fi

read -p "Press any key to continue... ${NC}" -n1 -s

# Hide Cursor
#apt-get -q=2 install --no-install-recommends unclutter > /dev/null
apt install unclutter-xfixes > /dev/null
# Install monit
apt-get -q=2 install --no-install-recommends monit > /dev/null


sed -i -e 's/xorg_installed=0/xorg_installed=1/g' stages.cfg
echo -e "\n${green}Done!${NC}"
else
	echo -e "${blue}Xorg already installed. Skipping...${NC}"
fi

read -p "Press any key to continue... ${NC}" -n1 -s


# Prevent sleeping for inactivity
echo -e "${red}Prevent sleeping for inactivity...${NC}"
if [ "$prevent_sleeping" == 0 ]
then
mkdir /etc/kbd
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/config -O /etc/kbd/config
systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Prevent sleeping already done. Skipping...${NC}"
fi

read -p "Press any key to continue... ${NC}" -n1 -s


echo -e "${red}Disabling root recovery mode...${NC}"
if [ "$grub_recovery_disable" == 0 ]
then
sed -i -e 's/GRUB_DISTRIBUTOR=`lsb_release -i -s 2> \/dev\/null || echo Debian`/GRUB_DISTRIBUTOR=Kiosk/g' /etc/default/grub
sed -i -e 's/GRUB_TIMEOUT=10/GRUB_TIMEOUT=4/g' /etc/default/grub
sed -i -e 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="video=efifb fbcon=rotate:1"/g' /etc/default/grub
sed -i -e 's/GRUB_CMDLINE_LINUX_DEFAULT=""/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/g' /etc/default/grub
update-grub
sed -i -e 's/grub_recovery_disable=0/grub_recovery_disable=1/g' stages.cfg
echo -e "\n${green}Done!${NC}"
else
	echo -e "${blue}Root recovery already disabled. Skipping...${NC}"
fi

read -p "Press any key to continue... ${NC}" -n1 -s

echo -e "${red}Creating kiosk user...${NC}"
if [ "$kiosk_created" == 0 ]
then
useradd kiosk -m -d /home/kiosk -p `openssl passwd -apr1 K10sk` -s /bin/bash
sed -i -e 's/kiosk_created=0/kiosk_created=1/g' stages.cfg
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Kiosk already created. Skipping...${NC}"
fi

read -p "Press any key to continue... ${NC}" -n1 -s

# Configure kiosk autologin
echo -e "${red}Configuring kiosk autologin...${NC}"
if [ "$kiosk_autologin" == 0 ]
then
sed -i -e 's/NODM_ENABLED=false/NODM_ENABLED=true/g' /etc/default/nodm
sed -i -e 's/NODM_USER=root/NODM_USER=kiosk/g' /etc/default/nodm
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/nodm -O /etc/init.d/nodm
sed -i -e 's/kiosk_autologin=0/kiosk_autologin=1/g' stages.cfg
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Kiosk autologin already configured. Skipping...${NC}"
fi
	

read -p "Press any key to continue... " -n1 -s

# Install Mozilla Firefox browser
echo -e "${red}Installing ${blue}Mozilla Firefox${red} browser...${NC}"
if [ "$firefox_installed" == 0 ]
then
#add-apt-repository ppa:ubuntu-mozilla-security/ppa
add-apt-repository ppa:mozillateam/ppa
echo -e 'Package: *'"\n"'Pin: release o=LP-PPA-mozillateam'"\n"'Pin-Priority: 1001' | tee /etc/apt/preferences.d/mozilla-firefox
echo -e 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
apt-get -q=2 update
apt-get -q=2 -y install --force-yes firefox > /dev/null
sed -i "\$auser_pref(\"browser.sessionstore.resume_from_crash\", false);" /home/kiosk/.mozilla/firefox/*default-release/prefs.js
sed -i -e 's/firefox_installed=0/firefox_installed=1/g' stages.cfg
echo -e "\n${green}Done!${NC}"
else
	echo -e "${blue}Mozilla Firefox already installed. Skipping...${NC}"
fi


read -p "Press any key to continue..." -n1 -s

# Kiosk scripts
echo -e "${red}Creating Kiosk Scripts...${NC}"
if [ "$kiosk_scripts" == 0 ]
then
mkdir /home/kiosk/.kiosk

read -p "Press any key to continue..." -n1 -s

# Create other kiosk scripts
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/configs/browser.cfg -O /home/kiosk/.kiosk/browser.cfg
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/killfirefox.sh -O /home/kiosk/killfirefox.sh
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/browser_killer.sh -O /home/kiosk/.kiosk/browser_killer.sh
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/configs/monitrc -O /etc/monit/monitrc
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/configs/browser_switches.cfg -O /home/kiosk/.kiosk/browser_switches.cfg
chmod 777 /home/kiosk/.kiosk/browser.cfg

# Create browser killer
apt-get -q=2 install --no-install-recommends xprintidle > /dev/null
chmod +x /home/kiosk/.kiosk/browser_killer.sh
chmod +x /home/kiosk/killfirefox.sh
sed '1d' /etc/crontab
echo "0 0     * * *   root    shutdown -r now" >> /etc/crontab
sed -i -e 's/kiosk_scripts=0/kiosk_scripts=1/g' stages.cfg
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Kiosk scripts already installed. Skipping...${NC}"
fi


read -p "Press any key to continue... " -n1 -s


echo -e "${red}Installing touchscreen support...${NC}"
if [ "$touchscreen_installed" == 0 ]
then
apt-get -q=2 install --no-install-recommends xserver-xorg-input-multitouch xinput-calibrator > /dev/null
sed -i -e 's/touchscreen_installed=0/touchscreen_installed=1/g' stages.cfg
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Touchscreen support already installed. Skipping...${NC}"
fi


read -p "Press any key to continue... " -n1 -s

echo -e "${red}Installing audio...${NC}"
if [ "$audio_installed" == 0 ]
then
apt-get -q=2 install --no-install-recommends alsa > /dev/null
adduser kiosk audio
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/asoundrc -O /home/kiosk/.asoundrc
chown kiosk.kiosk /home/kiosk/.asoundrc
sed -i -e 's/audio_installed=0/audio_installed=1/g' stages.cfg
echo -e "\n${green}Done!${NC}"
else
	echo -e "${blue}Audio already installed. Skipping...${NC}"
fi

echo -e "${red}Installing ImageMagick...${NC}"
if [ "$immagemagick_installed" == 0 ]
then
apt-get -q=2 install --no-install-recommends imagemagick > /dev/null
sed -i -e 's/immagemagick_installed=0/immagemagick_installed=1/g' stages.cfg
echo -e "\n${green}Done!${NC}"
else
	echo -e "${blue}ImageMagick already installed. Skipping...${NC}"
fi

echo -e "${red}Installing OpenVPN...${NC}"
if [ "$openvpn_installed" == 0 ]
then
apt-get -q=2 install --no-install-recommends openvpn > /dev/null
sed -i -e 's/openvpn_installed=0/openvpn_installed=1/g' stages.cfg
echo -e "\n${green}Done!${NC}"
else
	echo -e "${blue}OpenVPN already installed. Skipping...${NC}"
fi

read -p "Press any key to continue..." -n1 -s

echo -e "${red}Installing 3rd party software...${NC}"
if [ "$additional_software_installed" == 0 ]
then
apt-get -q=2 install pulseaudio > /dev/null
apt-get -q=2 install libvdpau* > /dev/null
apt-get -q=2 install alsa-utils > /dev/null
apt-get -q=2 install mc > /dev/null

wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/default.pa -O /etc/pulse/default.pa
sed -i -e 's/additional_software_installed=0/additional_software_installed=1/g' stages.cfg
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}3rd party software already installed. Skipping...${NC}"
fi

read -p "Press any key to continue..." -n1 -s


# Set correct user and group permissions for /home/kiosk
echo -e "${red}Set correct user and group permissions for ${blue}/home/kiosk${red}...${NC}"
if [ "$kiosk_permissions" == 0 ]
then
chown -R kiosk.kiosk /home/kiosk/
sed -i -e 's/kiosk_permissions=0/kiosk_permissions=1/g' stages.cfg
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/sudoers -O /etc/sudoers
echo -e "${green}Done!${NC}"
else
	echo -e "${blue}Permissions already set. Skipping...${NC}"
fi


read -p "Press any key to continue..." -n1 -s

# Create xsession
wget -q https://gitlab.com/jayarruda/frontline-painel/-/raw/main/scripts/xsession -O /home/kiosk/.xsession

# Choose kiosk name
kiosk_name=""
while [[ ! $kiosk_name =~ ^[A-Za-z0-9]+$ ]]; do
    echo -e "${green}Provide kiosk name (e.g. kiosk1):${NC}"
    read kiosk_name
done

old_hostname="$( hostname )"

if [ -n "$( grep "$old_hostname" /etc/hosts )" ]; then
    sed -i "s/$old_hostname/$kiosk_name/g" /etc/hosts
else
    echo -e "$( hostname -I | awk '{ print $1 }' )\t$kiosk_name" >> /etc/hosts
fi

sed -i "s/$old_hostname/$kiosk_name/g" /etc/hostname
echo -e "${blue}Kiosk hostname set to: ${kiosk_name}${NC}"

# Create plymouth scripts
wget -q https://gitlab.com/jayarruda/frontline-plymouth/-/archive/master/frontline-plymouth-master.tar.gz
tar xvaf frontline-plymouth-master.tar.gz
cd frontline-plymouth-master
make install
make set-default

echo -e "${green}Reboot?${NC}"
echo "Do you wish to reboot?"
select yn in "Yes" "No"; do
    case $yn in
        Yes) shutdown -r now;;
        No)  break;;
    esac
done