#!/bin/bash

set_static_ip() {
    # Discover network interfaces
    interfaces=$(ip -o link show | awk -F': ' '{print $2}' | grep -v "lo" | tr '\n' ' ')
    
    # Use dialog to let the user select the network interface
    interface=$(dialog --title "Select Network Interface" --menu "Choose your network interface:" 15 50 4 $interfaces 2>&1 >/dev/tty)
    
    # If the user cancels the selection, exit the function
    if [ $? -ne 0 ]; then
        dialog --msgbox "Operation cancelled." 6 30
        return
    fi

    # Use dialog to get IP, Subnet Mask, Gateway, Primary DNS, and Secondary DNS
    exec 3>&1
    user_input=$(dialog --title "Set Static IP" --form "Enter the network details:" 15 50 0 \
        "IP Address:" 1 1 "" 1 15 15 0 \
        "Subnet Mask:" 2 1 "" 2 15 15 0 \
        "Gateway:" 3 1 "" 3 15 15 0 \
        "Primary DNS:" 4 1 "" 4 15 15 0 \
        "Secondary DNS:" 5 1 "" 5 15 15 0 \
        2>&1 1>&3)
    exec 3>&-

    # Parse the user input
    ip_address=$(echo "$user_input" | sed -n '1p')
    subnet_mask=$(echo "$user_input" | sed -n '2p')
    gateway=$(echo "$user_input" | sed -n '3p')
    primary_dns=$(echo "$user_input" | sed -n '4p')
    secondary_dns=$(echo "$user_input" | sed -n '5p')

    # Backup the current Netplan configuration
    sudo cp /etc/netplan/00-installer-config.yaml /etc/netplan/00-installer-config.yaml.bak

    # Convert subnet mask to CIDR notation
    IFS=. read -r i1 i2 i3 i4 <<< "$subnet_mask"
    netmask_bits=$(( (i1 * 256 + i2) * 256 + i3))
    cidr=$(echo "obase=2;$netmask_bits" | bc | awk '{ print length }')

    # Write the new Netplan configuration
    sudo bash -c "cat > /etc/netplan/00-installer-config.yaml <<EOF
network:
  version: 2
  ethernets:
    $interface:
      dhcp4: no
      addresses:
        - $ip_address/$cidr
      gateway4: $gateway
      nameservers:
        addresses:
          - $primary_dns
          - $secondary_dns
EOF"

    # Apply Netplan configuration
    sudo netplan apply

    dialog --msgbox "Static IP has been set successfully!" 6 40
}

# Call the function
set_static_ip
